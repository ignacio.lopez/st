package co.st.api.cursos.facade;

import co.st.api.cursos.dto.CursoDTO;
import co.st.api.cursos.mapper.CursoMapper;
import co.st.api.cursos.model.Curso;
import co.st.api.cursos.service.CursoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CursoFacade {

    private final CursoService cursoService;
    private final CursoMapper cursoMapper;

    public CursoFacade(CursoService cursoService, CursoMapper cursoMapper) {
        this.cursoService = cursoService;
        this.cursoMapper = cursoMapper;
    }

    public List<CursoDTO> listarCursos() {
        return cursoMapper.toDto(this.cursoService.listarCursos());
    }

    public CursoDTO almacenarCurso(CursoDTO cursoDTO){
        Curso curso = cursoMapper.toEntity(cursoDTO);
        Curso respuesta = this.cursoService.almacenarCurso(curso);
        return cursoMapper.toDto(respuesta);
    }

}
