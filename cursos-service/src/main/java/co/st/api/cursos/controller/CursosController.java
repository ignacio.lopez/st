package co.st.api.cursos.controller;


import co.st.api.cursos.dto.CursoDTO;
import co.st.api.cursos.facade.CursoFacade;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api")
public class CursosController {

    protected Logger logger = Logger.getLogger(CursosController.class.getName());

    private final CursoFacade cursoFacade;

    public CursosController(CursoFacade cursoFacade) {
        this.cursoFacade = cursoFacade;
    }


    @GetMapping
    @ApiOperation(value = "Consulta todos los cursos disponibles")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Los cursos fueron consultados exitosamente", response = List.class),
            @ApiResponse(code = 400, message = "La petición es inválida"),
            @ApiResponse(code = 500, message = "Error interno al procesar la respuesta")})
    public ResponseEntity<List<CursoDTO>> getCursos() {
        logger.info("Cursos.getCursos");
        List<CursoDTO> cursos = this.cursoFacade.listarCursos();
        return ResponseEntity.ok(cursos);
    }

    @PostMapping("{curso}")
    @ApiOperation(value = "Registra un nuevo curso")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "El curso fue almacenado exitosamente", response = CursoDTO.class),
            @ApiResponse(code = 400, message = "La petición es inválida"),
            @ApiResponse(code = 500, message = "Error interno al procesar la respuesta")})
    public ResponseEntity<CursoDTO> almacenarCurso(@RequestBody CursoDTO curso) {
        logger.info("Cursos.saveCurso");
        CursoDTO respuesta = this.cursoFacade.almacenarCurso(curso);
        return ResponseEntity.ok(respuesta);
    }


}
