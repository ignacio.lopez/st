package co.st.api.usuarios.controller;


import co.st.api.usuarios.dto.UsuarioDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController
@RequestMapping("/api")
public class UsuariosController {

    protected Logger logger = Logger.getLogger(UsuariosController.class.getName());


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Consulta todos los usuarios disponibles")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Los usuarios fueron consultados exitosamente", response = UsuarioDTO.class),
            @ApiResponse(code = 400, message = "La petición es inválida"),
            @ApiResponse(code = 500, message = "Error interno al procesar la respuesta")})
    public ResponseEntity<UsuarioDTO> getUsuarios() {

        logger.info("Usuarios.getUsuarios");


        return ResponseEntity.ok(null);
    }

}
